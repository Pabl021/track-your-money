-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-04-2021 a las 06:54:37
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `trackyourmoney2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `monthly_budget` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `categories`
--

INSERT INTO `categories` (`id`, `user_id`, `type_id`, `description`, `monthly_budget`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Transfer', '0', NULL, NULL),
(2, NULL, 2, 'Transfer', '0', NULL, NULL),
(3, NULL, 3, 'Transfer', '0', NULL, NULL),
(4, 1, 1, 'premio de tiempos', '7000', NULL, NULL),
(5, 1, 1, 'dinero de rifa', '15000', NULL, NULL),
(6, 1, 1, 'juan me abono del carro', '50000', NULL, NULL),
(7, 1, 1, 'ingreso de ventas', '120000', NULL, NULL),
(8, 1, 1, 'horas extras', '16400', NULL, NULL),
(9, 1, 2, 'pago de recibos', '15000', NULL, NULL),
(10, 1, 2, 'Pago cuatrimestre', '14500', NULL, NULL),
(11, 1, 2, 'regalos', '5000', NULL, NULL),
(12, 1, 2, 'gastos innecesarios', '6700', NULL, NULL),
(13, 1, 3, 'transaccion a steven', '3810', NULL, NULL),
(14, 1, 3, 'abono de la moto', '14500', NULL, NULL),
(15, 1, 3, 'pago  del apartamento', '39900', NULL, NULL),
(16, 1, 3, 'deposito de mejengas', '10000', NULL, NULL),
(17, 1, 3, 'entrada a la fiesta', '6000', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `categories_user_id_foreign` (`user_id`),
  ADD KEY `categories_type_id_foreign` (`type_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `categories`
--
ALTER TABLE `categories`
  ADD CONSTRAINT `categories_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `categories_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
