-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-04-2021 a las 06:53:56
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `trackyourmoney2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `transactions`
--

CREATE TABLE `transactions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `account_id` bigint(20) UNSIGNED NOT NULL,
  `type_id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `amount` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_transaction` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `account_id`, `type_id`, `category_id`, `detail`, `amount`, `date_transaction`, `created_at`, `updated_at`) VALUES
(1, 1, 3, 1, 1, 'pasaje bus', '-200', '2021-04-13', '2021-04-13 10:38:30', '2021-04-13 10:38:30'),
(2, 1, 6, 2, 2, 'pasaje bus', '108200', '2021-04-13', '2021-04-13 10:38:30', '2021-04-13 10:38:30'),
(3, 1, 5, 1, 1, 'abono2', '-3400', '2021-04-12', '2021-04-13 10:39:27', '2021-04-13 10:39:27'),
(4, 1, 1, 2, 2, 'abono2', '1462000', '2021-04-12', '2021-04-13 10:39:27', '2021-04-13 10:39:27'),
(5, 1, 6, 1, 1, 'mes 2', '-12000', '2021-04-12', '2021-04-13 10:39:50', '2021-04-13 10:39:50'),
(6, 1, 1, 2, 2, 'mes 2', '12000', '2021-04-12', '2021-04-13 10:39:50', '2021-04-13 10:39:50'),
(7, 1, 3, 1, 1, 'fiesta Platanar', '-4000', '2021-04-12', '2021-04-13 10:40:25', '2021-04-13 10:40:25'),
(8, 1, 6, 2, 2, 'fiesta Platanar', '2164000', '2021-04-12', '2021-04-13 10:40:25', '2021-04-13 10:40:25'),
(9, 1, 2, 1, 1, 'pago mejenga', '-2000', '2021-04-12', '2021-04-13 10:41:13', '2021-04-13 10:41:13'),
(10, 1, 6, 2, 2, 'pago mejenga', '1240000', '2021-04-12', '2021-04-13 10:41:13', '2021-04-13 10:41:13'),
(11, 1, 4, 1, 1, 'materiales', '-4400', '2021-04-12', '2021-04-13 10:42:03', '2021-04-13 10:42:03'),
(12, 1, 6, 2, 2, 'materiales', '3132800', '2021-04-12', '2021-04-13 10:42:03', '2021-04-13 10:42:03'),
(13, 1, 1, 1, 1, 'abono 3', '-6000', '2021-04-19', '2021-04-13 10:42:30', '2021-04-13 10:42:30'),
(14, 1, 6, 2, 2, 'abono 3', '6000', '2021-04-19', '2021-04-13 10:42:30', '2021-04-13 10:42:30'),
(15, 1, 1, 1, 1, 'pago 3', '-100', '2021-04-11', '2021-04-13 10:45:43', '2021-04-13 10:45:43'),
(16, 1, 1, 2, 2, 'pago 3', '100', '2021-04-11', '2021-04-13 10:45:43', '2021-04-13 10:45:43');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `transactions_user_id_foreign` (`user_id`),
  ADD KEY `transactions_account_id_foreign` (`account_id`),
  ADD KEY `transactions_type_id_foreign` (`type_id`),
  ADD KEY `transactions_category_id_foreign` (`category_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `transactions`
--
ALTER TABLE `transactions`
  ADD CONSTRAINT `transactions_account_id_foreign` FOREIGN KEY (`account_id`) REFERENCES `accounts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_type_id_foreign` FOREIGN KEY (`type_id`) REFERENCES `types` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transactions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
