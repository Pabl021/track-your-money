-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 13-04-2021 a las 06:54:09
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `trackyourmoney2`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `monthly_budget` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `category_id`, `description`, `monthly_budget`, `created_at`, `updated_at`) VALUES
(1, 8, 'extras en DOLE', '9000', NULL, NULL),
(2, 4, 'premio de raspadita', '3000', NULL, NULL),
(3, 6, 'segundo abono juan', '10000', NULL, NULL),
(4, 9, 'pago de agua', '5400', NULL, NULL),
(5, 9, 'pago de luz', '13200', NULL, NULL),
(6, 11, 'regalo de mi hermana', '6500', NULL, NULL),
(7, 12, 'compra de parlante', '2600', NULL, NULL),
(8, 7, 'venta de carreta', '45000', NULL, NULL),
(9, 8, 'extras CMA', '12900', NULL, NULL),
(10, 12, 'compra piscina', '9000', NULL, NULL),
(11, 4, 'chances martes', '43000', '2021-04-13 10:48:35', '2021-04-13 10:48:35'),
(12, 7, 'me pagaron el telefono vendido', '43000', '2021-04-13 10:48:57', '2021-04-13 10:48:57'),
(13, 8, 'horas extras super D/D', '12400', '2021-04-13 10:49:18', '2021-04-13 10:49:18'),
(14, 9, 'pago internet', '16700', '2021-04-13 10:49:32', '2021-04-13 10:49:32');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sub_categories_category_id_foreign` (`category_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD CONSTRAINT `sub_categories_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
