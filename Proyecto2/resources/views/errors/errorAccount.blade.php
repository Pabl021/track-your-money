@extends('layouts.menu')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Errors</title>
    <link rel="stylesheet" type="text/css" href="/css/transactions/index.css" />
</head>
<body>
    <style>
        option,
        select {
            font-size: 16px;
            background-color: #B0F5F5;
        }

        .box {
            top: 50%;
            left: 50%;
            text-align: center;
            margin-top: 13vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }

        .card-body,
        .card-header {
            color: white;
        }

        .box input[type="text"],
        .box input[type="number"],
        .box input[type="date"] {
            color: white;
            Background-color: #000000;
            font-weight: bold;
        }
    </style>
</body>
</html>
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box">
                    <div class="card-header">{{ __('') }}</div>
                    <div class="card-body">
                       {{$msjError}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection