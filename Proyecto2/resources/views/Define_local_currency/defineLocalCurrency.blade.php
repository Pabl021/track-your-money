@extends('layouts.menu')
<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
    <style>
        body {
            background: url(https://savantwealth.com/wp-content/uploads/2020/03/Focus-on-the-Fundamentals-Tile.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
        }

        .box {
            top: 50%;
            left: 50%;
            text-align: center;
            margin-top: 3vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }

        .box input[type="email"],
        .box input[type="password"],
        .box input[type="text"] {
            color: #FD7300;
            Background-color: #000000;
            font-weight: bold;
        }

        .card-body,
        .card-header {
            color: white;
        }
    </style>
</body>
</html>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="box">

                <div class="card-header">{{$message}}</div>

                <div class="card-body">
                    <form method="POST" action="{{route('save_coin')}}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Symbol') }}</label>

                            <div class="col-md-6">
                                <input id="symbol" type="text"
                                    class="form-control @error('symbol') is-invalid @enderror" name="symbol"
                                    value="{{ old('symbol') }}" required autocomplete="symbol">

                                @error('symbol')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <input id="description" type="text"
                                    class="form-control @error('description') is-invalid @enderror" name="description"
                                    value="{{ old('description') }}" required autocomplete="description">

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for=""
                                class="col-md-4 col-form-label text-md-right">{{ __('Interest Rate') }}</label>

                            <div class="col-md-6">
                                <input id="interestRate" type="text"
                                    class="form-control @error('interestRate') is-invalid @enderror" name="interestRate"
                                    required autocomplete="interestRate">

                                @error('interestRate')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-outline-success ">
                                    {{ __('Save') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="box">
                <div class="card-header">{{ __('My coins') }}</div>

                <div class="card-body">
                    <table class="table table-dark">
                        <thead>
                            <tr>

                                <th scope="col">Name</th>
                                <th scope="col">Interest Rate</th>
                                <th scope="col">Options</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($coins as $coin)
                            <tr>
                                <td>{{$coin->name}}</td>
                                <td>{{$coin->rate}}</td>
                                <td>
                                    <div class="col-auto" style="display: inline-block">
                                        <form method="POST" action="{{ route('deleteCoin',['id'=>$coin->id]) }}">
                                            @method('DELETE')
                                            @csrf
                                            {{-- <a href="{{ route('deleteCoin',['id'=>$coin->id]) }}" class="btn
                                            btn-warning" >Delete</a> --}}
                                            <button type="submit" class="btn btn-outline-danger">Delete</button>
                                        </form>
                                    </div>
                                    <div class="col-auto" style="display: inline-block">


                                        <button type="button" id="btnModalUpdate" class="btn btn-outline-info"
                                            data-toggle="modal" data-id="{{ $coin->id}}" data-target="#exampleModal">
                                            Update
                                        </button>
                                    </div>
                                </td>

                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel" style="color:#87CEFA;">Edit coins</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="color:#87CEFA;">
            <form method="POST" action="" id="editFormCoin">
                @csrf
                @method('PUT')

                    <div class="form-group row">
                        <label for="nameUpdate" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                        <div class="col-md-6">
                            <input id="nameUpdate" type="text" class="form-control @error('name') is-invalid @enderror"
                                name="nameUpdate" required autocomplete="name" autofocus>

                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="descriptionUpdate"
                            class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                        <div class="col-md-6">
                            <input id="descriptionUpdate" type="text"
                                class="form-control @error('description') is-invalid @enderror" name="descriptionUpdate"
                                required autocomplete="description">

                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Interest Rate') }}</label>

                        <div class="col-md-6">
                            <input id="interestRateUpdate"  type="number"
                                class="form-control @error('interestRate') is-invalid @enderror" name="rateUpdate"
                                required autocomplete="interestRate">

                            @error('interestRate')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Local') }}</label>

                        <div class="col-md-6">
                            <select id="isLocal" class="form-select form-select-lg mb-3"  name="isLocal">
                                    <option value="1">Local</option>
                                    <option value="0">Foreign</option>
                            </select>

                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        </div>
                    </div>
                
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript">
    $(document).on("click", "#btnModalUpdate", function(){
        var id = $(this).data('id');
        console.log("ID: "+ id); 
        $.ajax({                        
           type: "get",                 
           url: 'updateCoin',                     
           //dataType: 'json', 
           data: { id }, 
           success: function(data)             
           {
                data = JSON.parse(data);
                console.log(data.name);

                document.getElementById('nameUpdate').value=data.name;
                document.getElementById('descriptionUpdate').value=data.description;
                document.getElementById('interestRateUpdate').value=data.rate;

                var valueSelected = $("#isLocal").val();
                if(valueSelected  != data.is_local){
                    let element = document.getElementById("isLocal");
                    element.value = data.is_local;
                }

                var route = "{{ route('update') }}"+"/"+data.id;
                    $("#editFormCoin").attr('action', route);   
           }
       });
    });
</script>
@endsection



<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
