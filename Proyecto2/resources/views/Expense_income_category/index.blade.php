@extends('layouts.menu')

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <style>
        body {
            background: url(https://savantwealth.com/wp-content/uploads/2020/03/Focus-on-the-Fundamentals-Tile.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;


        }

        .box {
            top: 50%;
            left: 50%;
            text-align: center;
            margin-top: 13vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }

        .box input[type="email"],
        .box input[type="password"],
        .box input[type="text"] {
            color: #FD7300;
            Background-color: #000000;
            font-weight: bold;
        }

        .card-body,
        .card-header {
            color: white;
        }
    </style>
</body>

</html>


@section('content')


<div class="container">
    <div class="row">
        <div id="divCol" class="col-md-12">
            <nav class="navbar navbar-expand-lg" style="background-color: transparent;">
                <a class="navbar-brand" href="#" style=" color:white;">
                    <h1>Category or Sub Category</h1>
                </a>
                <ul class="navbar-nav mr-auto">
                </ul>
                <a href="{{ route('indexCategory') }}">
                <button type="button" class="btn btn-outline-dark">
                    See categories
                </button>
                </a>
                
                <button type="button" class="btn btn-outline-dark" data-toggle="modal" data-target="#exampleModal">
                    Create category
                </button>
            </nav>
        </div>
    </div>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box">
                    <div class="card-header">{{ __('Expensive/Income Category') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{route('insertSubCategory')}}">
                            @csrf

                            <div class="form-group row">
                                <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>

                                <div class="col-md-6">

                                    <select name="typeCat" class="form-control" id="exampleFormControlSelect1">
                                        @foreach($categories as $cat)
                                        <option value="{{$cat->id}}">{{$cat->description}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                                <div class="col-md-6">
                                    <input id="description" type="text" class="form-control @error('description') is-invalid @enderror" name="description" required autocomplete="description">

                                    @error('description')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Budget') }}</label>

                                <div class="col-md-6">
                                    <input id="budget" type="text" class="form-control @error('budget') is-invalid @enderror" name="monthlySubCat" required autocomplete="budget">

                                    @error('budget')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>



                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-outline-success ">
                                        {{ __('Save') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Create category</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{route('insertCategory')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="category" class="col-form-label">Category</label>
                            <input type="text" class="form-control" id="category" name="categoryName">
                        </div>
                        <div class="form-group">
                            <label for="monthly" class="col-form-label">Monthly budget</label>
                            <input type="text" class="form-control" id="monthly" name="monthly">
                        </div>
                        <div class="form-group">
                            <label for="category" class="col-form-label">Type</label>
                            <select name="type" class="form-control" id="exampleFormControlSelect">
                                @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                                @endforeach
                            </select>
                        </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>

                </div>
                </form>
            </div>
        </div>
    </div>





    @endsection

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>