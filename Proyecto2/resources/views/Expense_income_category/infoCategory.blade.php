@extends('layouts.menu')

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <style>
        body {
            background: url(https://savantwealth.com/wp-content/uploads/2020/03/Focus-on-the-Fundamentals-Tile.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;


        }

        .box {
            top: 50%;
            left: 50%;
            text-align: center;
            margin-top: 13vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }

        .box input[type="email"],
        .box input[type="password"],
        .box input[type="text"] {
            color: #FD7300;
            Background-color: #000000;
            font-weight: bold;
        }

        .card-body,
        .card-header {
            color: white;
        }
    </style>
</body>

</html>


@section('content')


<div class="container">
    <div class="row">
        <div id="divCol" class="col-md-12">
            <nav class="navbar navbar-expand-lg" style="background-color: transparent;">
                <a class="navbar-brand" href="#" style=" color:white;">
                    <h1>Information Category or Sub Category</h1>
                </a>
                <ul class="navbar-nav mr-auto">
                </ul>
                <a href="{{ route('category') }}">
                <button type="button" class="btn btn-outline-dark">
                    Create categories or sub categories
                </button>
                </a>
            </nav>
        </div>
    </div>

    


    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box">
                    <div class="card-header">{{ __('My categories') }}</div>

                    <div class="card-body">
                        <table class="table table-dark">
                            <thead>
                                <tr>

                                    <th scope="col">Name</th>
                                    <th scope="col">Monthly budget</th>
                                    <th scope="col">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $cate)
                                <tr>

                                    <td>{{$cate->description}}</td>
                                    <td>{{$cate->monthly_budget}}</td>
                                    <td>
                                        <div class="col-auto" style="display: inline-block">
                                            <form method="POST" action="{{ route('deleteCategory',['id'=>$cate->id]) }}">
                                                @method('DELETE')
                                                @csrf
                                                {{-- <a href="{{ route('deleteCategory',['id'=>$cate->id]) }}" class="btn btn-warning" >Delete</a> --}}
                                                <button type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </div>
                                        <div class="col-auto" style="display: inline-block">


                                            <button type="button" id="btnModalUpdateCategory" class="btn btn-outline-info" data-toggle="modal" data-id="{{$cate->id}}" data-target="#exampleModalCat">
                                                Update
                                            </button>
                                        </div>
                                    </td>

                                </tr>

                                @endforeach
                            </tbody>
                        </table>

                        <div class="card-header">{{ __('My sub categories') }}</div>
                        <table class="table table-dark">
                            <thead>
                                <tr>

                                    <th scope="col">Name</th>
                                    <th scope="col">Monthly budget</th>
                                    <th scope="col">Options</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $c)
                                @foreach($c->subcategories as $subCat)
                                <tr>

                                    <td>{{$subCat->description}}</td>
                                    <td>{{$subCat->monthly_budget}}</td>
                                    <td>
                                        <div class="col-auto" style="display: inline-block">
                                            <form method="POST" action="{{ route('deleteSubCategory',['id'=>$subCat->id]) }}">
                                                @method('DELETE')

                                                @csrf
                                                {{-- <a href="{{ route('deleteSubCategory',['id'=>$subCat->id]) }}" class="btn btn-warning" >Delete</a> --}}
                                                <button type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </div>
                                        <div class="col-auto" style="display: inline-block">


                                            <button type="button" id="btnModalUpdateSubCategory" class="btn btn-outline-info" data-toggle="modal" data-id="{{$subCat->id}}" data-target="#exampleModalSub">
                                                Update
                                            </button>
                                        </div>
                                    </td>

                                </tr>
                                @endforeach
                                @endforeach

                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="modal fade" id="exampleModalCat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color:#87CEFA;">Edit Categories</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="" id="editFormCategory">
                @csrf
                @method('PUT')
                <div class="modal-body" style="color:#87CEFA;">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Type') }}</label>

                        <div class="col-md-6">                           

                            <select name="typeCatUpdate" class="form-control" id="typeCat">
                                @foreach($types as $type)
                                <option value="{{$type->id}}">{{$type->name}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                        <div class="col-md-6">
                            <input id="descriptionCateUpdate" type="text" class="form-control @error('description') is-invalid @enderror" name="descriptionUpdate" required autocomplete="description">

                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Budget') }}</label>

                        <div class="col-md-6">
                            <input id="budgetCateUpdate" type="text" class="form-control @error('budget') is-invalid @enderror" name="monthlyCatUpdate" required autocomplete="budget">

                            @error('budget')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).on("click", "#btnModalUpdateCategory", function() {
            var id = $(this).data('id');
            console.log("ID: " + id);
            $.ajax({
                type: "get",
                url: 'getCategory',
                data: {
                    id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    document.getElementById('descriptionCateUpdate').value = data.description;
                    document.getElementById('budgetCateUpdate').value = data.monthly_budget;

                    var valueSelected = $("#typeCat").val();
                    if(valueSelected  != data.type_id){
                        let element = document.getElementById("typeCat");
                        element.value = data.type_id;
                        var route = "{{ route('updateCategory') }}"+"/"+data.id;
                        $("#editFormCategory").attr('action', route); 
                    }
                    var route = "{{ route('updateCategory') }}"+"/"+data.id;
                    $("#editFormCategory").attr('action', route); 

                }
            });
        });
    </script>







<div class="modal fade" id="exampleModalSub" tabindex="-1" role="dialog" aria-labelledby="exampleModalSub" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel" style="color:#87CEFA;">Edit Sub Categories</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" action="" id="editFormSubCategory">
                @csrf
                @method('PUT')
                <div class="modal-body" style="color:#87CEFA;">
                    <div class="form-group row">
                        <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>

                        <div class="col-md-6">                           

                            <select name="typeSubUpdate" class="form-control" id="typeSub">
                                @foreach($categories as $cate)
                                <option value="{{$cate->id}}">{{$cate->description}}</option>
                                @endforeach
                            </select>

                        </div>
                    </div>



                    <div class="form-group row">
                        <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                        <div class="col-md-6">
                            <input id="descriptionSubUpdate" type="text" class="form-control @error('description') is-invalid @enderror" name="descriptionSubUpdate" required autocomplete="description">

                            @error('description')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="" class="col-md-4 col-form-label text-md-right">{{ __('Budget') }}</label>

                        <div class="col-md-6">
                            <input id="budgetSubUpdate" type="text" class="form-control @error('budget') is-invalid @enderror" name="monthlySubCatUpdate" required autocomplete="budget">

                            @error('budget')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
              
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        $(document).on("click", "#btnModalUpdateSubCategory", function() {
            var id = $(this).data('id');
            console.log("ID: " + id);
            $.ajax({
                type: "get",
                url: 'getSubCategory',
                data: {
                    id
                },
                success: function(data) {
                    data = JSON.parse(data);
                    console.log(data)
               
                    document.getElementById('descriptionSubUpdate').value = data.description;
                    document.getElementById('budgetSubUpdate').value = data.monthly_budget;
                    
                    var valueSelected = $("#typeSub").val();
                    if(valueSelected  != data.category_id){
                        let element = document.getElementById("typeSub");
                        element.value = data.category_id;
                        var route = "{{ route('updateSubCategory') }}"+"/"+data.id;
                        $("#editFormSubCategory").attr('action', route); 
                    }
                    var route = "{{ route('updateSubCategory') }}"+"/"+data.id;
                    $("#editFormSubCategory").attr('action', route);    
                    
                }
            });
        });
    </script>





    @endsection

    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>