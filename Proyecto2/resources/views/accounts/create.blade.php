<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">New account</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="{{route('createAccount')}}">
            @csrf
            <div class="modal-body">
                <div class="mb-3">
                    <label for="name" class="form-label">Name</label>
                    <input required type="text" name="name" class="form-control" id="name" placeholder="Name for account">
                </div>
                <div class="mb-3">
                    <label for="description" class="form-label">Description</label>
                    <input required type="text" name="description" class="form-control" id="description" placeholder="Description for account">
                </div>
                <div class="mb-3">
                    <label for="monthly" class="form-label">Monthly budget</label>
                    <input required type="number" name="monthly" class="form-control" id="monthly" placeholder="Monthly budget">
                </div>
                <label for="coin" class="form-label">Choose coin for account</label>
                <select id="coin" class="form-select form-select-lg mb-3"  name="coin">
                    @foreach ($user->coins as $c)
                        <option value="{{$c->id}}">{{$c->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                {{-- data-toggle="modal" data-target="#exampleModalCenter" --}}
                    <button type="submit" class="btn btn-success" >
                        Save
                    </button>
            </div>
        </form>
      </div>
    </div>
  </div>