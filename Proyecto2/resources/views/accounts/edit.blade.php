  <div class="modal fade" id="editAccount" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalCenterTitle">Edit account</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" action="" id="editFormAccount">
          @csrf
          @method('PUT')
            <div class="modal-body">
                <div class="mb-3">
                    <label for="nameUpdate" class="form-label">Name edit</label>
                    <input required type="text"  name="name" class="form-control" id="nameUpdate" placeholder="Name for account">
                </div>
                <div class="mb-3">
                    <label for="descriptionUpdate" class="form-label">Description</label>
                    <input required type="text" name="description" class="form-control" id="descriptionUpdate" placeholder="Description for account">
                </div>
                <div class="mb-3">
                    <label for="monthlyUpdate" class="form-label">Monthly budget</label>
                    <input required type="number" name="monthly" class="form-control" id="monthlyUpdate" placeholder="Monthly budget">
                </div>
                <label for="coin" class="form-label">Choose coin for account</label>
                <select id="coinUpdate" class="form-select form-select-lg mb-3"  name="coin">
                    @foreach ($user->coins as $c)
                        <option value="{{$c->id}}">{{$c->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">
                        Edit
                    </button>
                </a>
            </div>
        </form>
      </div>
    </div>
  </div>