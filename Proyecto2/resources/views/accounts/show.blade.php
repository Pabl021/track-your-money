@if (!empty($accounts))
    @foreach ($accounts as $a)
        <div class="card-group" style="padding: 0.3%;"  >
            {{-- <div class="font-icon-list col-lg-2 col-md-3 col-sm-4 col-xs-6 col-xs-6"> --}}
                <div id="card" class="card" style="background-color: black; opacity:70%; "  >
                    <div class="card-body" style="color:white; ">
                        <h5 class="card-title text-info" style=" text-align: center;">{{$a->name}}</h5>
                        <p class="card-text" style=" text-align: center;">{{$a->description}}</p>
                        <p class="card-text" style=" text-align: center;">{{$a->monthly_budget}}</p>
                        <p class="card-text" style=" text-align: center;">Using coin: {{$a->coin->name}}</p>

                        <div  class="col-auto" style="display: inline-block">
                            <form method="POST" action="{{ route('deleteAccount',['id'=>$a->id]) }}">
                                @method('DELETE')
                                @csrf
                                {{-- <a href="{{ route('deleteAccount',['id'=>$a->id]) }}"  class="btn btn-warning" style="display: inline-block">Delete</a> --}}
                                    <button type="submit"  class="btn btn-outline-danger">Delete</button>
                            </form>
                        </div>
                        <div   class="col-auto" style="display: inline-block">
                        {{-- <a href="/{{$a->id}}" class="btn btn-dark" style="display: inline-block">Update</a> --}}
                         <button type="button" class="btn btn-outline-info" id="btnModalUpdate" style="display: inline-block" 
                         data-id="{{$a->id}}"  data-toggle="modal" data-target="#editAccount">
                            Update
                        </button>

                            {{-- <button type="submit"  class="btn btn-warning">Update</button> --}}

                        </div>

                    </div>
                </div>
            </div>
            {{-- si se desean ver las transaciones por cuenta agregada --}}
            {{-- <h2>Transactions</h2>
            @foreach ($a->transactions as $t)
                <p>Category: {{$t->category->description}}</p>
                <p>Account: {{$t->account->name}}</p>
                <p>Detail: {{$t->detail}}</p>
            @endforeach --}}
    @endforeach
@endif