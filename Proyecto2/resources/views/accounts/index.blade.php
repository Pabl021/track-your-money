@extends('layouts.menu')

<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

   
</head>
<body>

<style>

</style>

</body>
</html>

@section('content')
<div class="container">
    <div  class="row">
        <div id="divCol" class="col-md-12">

<nav class="navbar navbar-expand-lg" style="background-color: transparent;">
   <a class="navbar-brand" href="#"style=" color:white;"> <h1>My Accounts</h1> </a> 
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        
        
        </ul>
        <form class="form-inline my-2 my-lg-0">
        <button type="button" class="btn btn-info" id="btnModal"
                    data-toggle="modal" data-target="#exampleModalCenter">
                            New Account
                    </button>
        <input id="txtFilter" class="form-control me-2" type="search"  placeholder="Search" aria-label="Search">
                        {{-- <button class="btn btn-outline-success" type="submit">Search</button> --}}
                    
        </form>
    </div>
</nav>
    
           
        </div>

        
    </div>
    <div id="cardHeader"  class="card-header">
                <div  class="card-body all-icons">
                    <div id="resultado" class="row" >
                       @include('accounts.show')
                    </div>
                </div>
                {{$accounts->links('vendor.pagination.bootstrap-4')}}
            </div>
    </div>
</div>
@include('accounts.create')
@include('accounts.edit')

<script>
    window.addEventListener("load",function(){
        document.getElementById("txtFilter").addEventListener("keyup", function(){
            fetch(`/accountFilter?text=${document.getElementById("txtFilter").value}`,{
                 method:'get'
            })
            .then(response=> response.text())
            .then(html=>{
                document.getElementById("resultado").innerHTML=html;
            })
            .catch(error=> console.error(error));
        });
    });

</script>

<script type="text/javascript">
    $(document).on("click", "#btnModalUpdate", function(){
        var id = $(this).data('id');
        $.ajax({                        
           type: "get",                 
           url: 'account',                     
           //dataType: 'json', 
           data: { id }, 
           success: function(data)             
           {
                data = JSON.parse(data);
                document.getElementById('nameUpdate').value=data.name;
                document.getElementById('descriptionUpdate').value=data.description;
                document.getElementById('monthlyUpdate').value=data.monthly_budget;
                var valueSelected = $("#coinUpdate").val();
                if(valueSelected  != data.id){
                    let element = document.getElementById("coinUpdate");
                    element.value = data.coin_id;
                    var route = "{{ route('updateAccount') }}"+"/"+data.id;
                    console.log(route); 
                    $("#editFormAccount").attr('action', route); 
                }
           }
       });
    });
</script>
@endsection
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
