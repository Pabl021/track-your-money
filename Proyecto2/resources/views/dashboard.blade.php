<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Dashboard</title>
</head>
<body>
    <a href="/myCoins">My coins</a><br>
    <a href="/accounts">My accounts</a><br>
    <a href="/transactions">My transactions</a><br>
    <a href="/type">Type</a><br>

    @if (!empty($user))
       @foreach ($user->coins as $c)
           <h3>{{$c->name}}</h3>
       @endforeach
    @endif

    @if (!empty($accounts))
        @foreach ($accounts as $a)
            <h3>{{$a->name}}</h3>
            <p>Using coin: {{$a->coin->name}}</p> 

            <h2>Transactions</h2>
            @foreach ($a->transactions as $t)
                <p>Category: {{$t->category->description}}</p>
                <p>Account: {{$t->account->name}}</p>
                <p>Detail: {{$t->detail}}</p>
            @endforeach
        @endforeach
    @endif

    @if (!empty($types))
        <h2>Type: {{$types->name}}</h2>
        @foreach ($types->categories as $c)
            <h3> Category {{$c->description}}</h3>
            @foreach ($c->subcategories as $s)
                <p>{{$s->description}}</p> 
            @endforeach
        @endforeach

        <h2>Transactions</h2>
        @foreach ($types->transactions as $t)
            <p>Account: {{$t->account->name}}</p>
            <p>Detail: {{$t->detail}}</p>
        @endforeach
    @endif

    @if (!empty($transaction))
       {{-- @foreach ($transactions as $t) --}}
           <h3>Detail: {{$transaction->detail}}</h3>
           <h3>Account: {{$transaction->account->name}}</h3>
           <h3>Category: {{$transaction->category->description}}</h3>


       {{-- @endforeach --}}
    @endif


    
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
crossorigin="anonymous"></script>

</body>
</html>