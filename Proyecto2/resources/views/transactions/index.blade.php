@extends('layouts.menu')
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transactions</title>
    <link rel="stylesheet" type="text/css" href="/css/transactions/index.css" />
</head>
<body>
    <style>
        option,
        select {
            font-size: 16px;
            background-color: #B0F5F5;
        }

        .box {
            top: 50%;
            left: 50%;
            text-align: center;
            margin-top: 13vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }

        .card-body,
        .card-header {
            color: white;
        }

        .box input[type="text"],
        .box input[type="number"],
        .box input[type="date"] {
            color: white;
            Background-color: #000000;
            font-weight: bold;
        }
    </style>
</body>
</html>
@section('content')
    @include('transactions.header')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="box">
                    <div class="card-header">{{ __('MY TRANSACTIONS') }}</div>
                    <div class="card-body">
                        <form action="{{ route('save')}}" method="POST">
                            @csrf
                            @if (!empty($expensive))
                                    
                                <input type="hidden" value=" {{$expensive->id}}" name="type">
                                <input type="hidden" value=" {{'isExpensive'}}" name="isType">

                                <select id="expensives" class="form-select form-select-lg mb-3" name="category">
                                    @foreach ($expensive->categories as $c)
                                        @if ($c->user_id)
                                            <option value="{{$c->id}}">{{$c->description}}</option>
                                        @endif
                                    @endforeach
                                </select>
                            @endif

                            @if (!empty($income))
                                <input type="hidden" value=" {{$income->id}}" name="type">
                                <input type="hidden" value=" {{'isIncome'}}" name="isType">

                                <select id="incomes" class="form-select form-select-lg mb-3" name="category"">
                                @foreach ($income->categories as $c)
                                    @if ($c->user_id)
                                        <option value=" {{$c->id}}">{{$c->description}}</option>
                                    @endif
                                @endforeach
                                </select>
                            @endif

                            @if (!empty($transaction))
                                <input type="hidden" value=" {{$transaction->id}}" name="type">
                                <input type="hidden" value=" {{'isTransaction'}}" name="isType">

                                <select id="incomes" class=" form-select form-select-lg mb-3" name="category"">
                                @foreach ($transaction->categories as $t)
                                    <option value=" {{$t->id}}">{{$t->description}}</option>
                                @endforeach
                                </select>


                            <label for="accounts">Account to deposit</label>
                            <select id="accountDeposit" class="form-select form-select-lg mb-3" name="accountDeposit">
                                @foreach ($accountWithCoinLocal as $a)
                                        <option value="{{$a->id}}">{{$a->name}}</option>
                                @endforeach
                            </select>
                            @endif


                            @if (!empty($user))
                                <label for="accounts">Accounts</label>
                                <select id="accounts" class="form-select form-select-lg mb-3" name="accounts">
                                @foreach ($user->accounts as $a)
                                    <option value="{{$a->id}}">{{$a->name}}</option>
                                @endforeach
                                </select>
                            @endif


                            <div class="form-group row">
                                <label for="Amount"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Amount') }}</label>
                                <div class="col-md-6">
                                    <input required type="number" placeholder="Write amount" name="amount" id="amount" 
                                    min="1" pattern="^[0-9]+" oninput="this.value = Math.max(this.value, 1)"><br>
   
                                    @if (session('error'))
                                    <span style="color:red;" class="help-block"> {{session('error')}}</span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Detail"
                                    class="col-md-4 col-form-label text-md-right">{{ __('Detail') }}</label>
                                <div class="col-md-6">
                                    <input required type="text" placeholder="Write detail" name="detail" id="detail">
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="Date" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>
                                <div class="col-md-6">
                                    <input required type="date" name="dateTransaction" id="date">
                                    
                                </div>
                            </div>

                            <div>
                                <button class="btn btn-primary" type="submit">Save</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection