<div class="container">
    <div class="row">
        <div id="divCol" class="col-md-12">
            <nav class="navbar navbar-expand-lg" style="background-color: transparent;">
                <a class="navbar-brand" href="#" style=" color:white;">
                    <h1>My Transactions</h1>
                </a>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                    </ul>
                    <a href="{{ route('transactions')}}">
                        <button type="button" class="btn btn-outline-dark">Transactions</button>
                    </a>
                    <a href="{{ route('expensives')}}">
                        <button type="button" class="btn btn-outline-dark">Expensive</button>
                    </a>
                    <a href="{{ route('incomes')}}">
                        <button type="button" class="btn btn-outline-dark">Income</button>
                    </a>
                    <a href="{{ route('allTransactions')}}">
                        <button type="button" class="btn btn-outline-dark">Show Transactions</button>
                    </a>
                </div>
            </nav>
        </div>
    </div>