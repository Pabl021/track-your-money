@extends('layouts.menu')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transactions</title>
    <link rel="stylesheet" type="text/css" href="/css/transactions/index.css" />
</head>

<body>
    <style>
        option,
        select {
            font-size: 16px;
            background-color: #B0F5F5;
        }

        .box {
            top: 10%;
            left: 50%;
            text-align: center;
            margin-top: 13vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }
        .transactions{
            top: 50%;
            left: 100%;
            width: 100%;
            text-align: center;
            margin-top: 13vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }

        .card-body,
        .card-header {
            color: white;
        }

        .box input[type="text"],
        .box input[type="number"],
        .box input[type="date"] {
            color: white;
            Background-color: #000000;
            font-weight: bold;
        }
    </style>
</body>

</html>
@section('content')
@include('transactions.header')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <div class="box">
                <div class="card-header">{{ __('MY TRANSACTIONS') }}</div>
                <div class="card-body">
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th scope="col">Account</th>
                                <th scope="col">Type of</th>
                                <th scope="col">Category</th>
                                <th scope="col">Detail</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Date transaction</th>
                                <th scope="col">Options</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($allTransactions as $t)
                                <tr>
                                    <td>{{$t->account->name}}</td>
                                    <td>{{$t->type->name}}</td>
                                    <td>{{$t->category->description}}</td>
                                    <td>{{$t->detail}}</td>
                                    <td>{{$t->amount}}</td>
                                    <td>{{ \Carbon\Carbon::parse($t->date_transaction)->isoFormat('MMM Do YYYY')}}</td>
                                    <td>
                                        <div class="col-auto" style="display: inline-block">
                                            <form method="POST" action="{{ route('deleteTransaction',['id'=>$t->id]) }}">
                                                @method('DELETE')
                                                @csrf
                                                <button type="submit" class="btn btn-outline-danger">Delete</button>
                                            </form>
                                        </div>
                                            <div class="col-auto" style="display: inline-block">
                                            {{-- <button type="button" id="btnModalUpdate" class="btn btn-outline-info"
                                                data-toggle="modal" data-id="" data-target="#exampleModalCat">
                                                Update
                                            </button> --}}

                                                <a class="btn btn-outline-info"
                                                 href="{{ route('updateTransaction',['id'=>$t->id]) }}
                                                 "> Update</a>
                                         </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                {{$allTransactions->links('vendor.pagination.bootstrap-4')}}
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection