@extends('layouts.menu')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transactions</title>
    <link rel="stylesheet" type="text/css" href="/css/transactions/index.css" />

</head>

<body>
    <style>
      

        .box {
            top: 50%;
            left: 50%;
            text-align: center;
            margin-top: 13vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }

        .card-body,
        .card-header {
            color: white;
        }

        .box input[type="text"],
        .box input[type="number"],
        .box input[type="date"] {
            color: white;
            Background-color: #000000;
            font-weight: bold;
        }
    </style>
</body>

</html>
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-16">
            <a class="btn btn-primary"  
         href="{{route('graphics')}}">Regresar</a>
                
            <div class="box">
                
                <div class="card-header">{{ __('TRANSACTION ') }}</div>
                 <div class="card-body">
                    <table class="table table-dark">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Account</th>
                                <th scope="col">Detalle</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Date transaction</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Category->transactions as $t)
                                <tr>
                                    <td>{{$Category->description}}</td>
                                    <td>{{$t->account->name}}</td>
                                    <td>{{$t->detail}}</td>
                                    <td>{{$t->amount= $t->amount<0?($t->amount*-1):$t->amount}}</td>
                                    <td>{{$t->date_transaction}}</td>
                                </tr>
                            @endforeach
                               
                        </tbody>
                    </table>

                   
                </div>
                
            </div>
            @if (count($Category->subcategories)>0)
            <div class="col-md-6">
                <input type="hidden" value="{{$Category->id}}" id="idCategory">
                <div class="card" style="top: 5vh;">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="mt-2">Sub Categories</h4>
                            </div>
                            <div class="col-md-6">
                                <ul class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link platform active" data-toggle="pill" id="subCategory"
                                            href="#" role="tab" aria-controls="pills-home" aria-selected="true">See</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas id="subCategoryChart" width="75"></canvas>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </div>
</div>

<script type="text/javascript">
    const category = @json($Category);
    var subCategoryCtx = document.getElementById('subCategoryChart').getContext('2d');
    var subCategoryChart = new Chart(subCategoryCtx);

    valuesAll = [];
    labels = [];
    category.subcategories.forEach(e => {
        valuesAll.push(e.monthly_budget)
        labels.push(e.description)
    });

    loadSubCategoriesChart(valuesAll, labels);
    
    function loadSubCategoriesChart(data, labels) {
        subCategoryChart.destroy();
        subCategoryChart = new Chart(subCategoryCtx, {
            type: 'pie',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Sub Categories',
                    data: data,
                    borderWidth: 1,
                    backgroundColor: ['#dc3545', '#ffc107', '#17a2b8', '#28a745', '#666666', '#000000']
                }],
            },
            options: {
                plugins: {
                   
                },
                
            }
        });
    }



</script>

@endsection
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.2/dist/chart.min.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>