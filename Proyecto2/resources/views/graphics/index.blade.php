@extends('layouts.menu')
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Transactions</title>
    <link rel="stylesheet" type="text/css" href="/css/transactions/index.css" />

</head>

<body>
    <style>
      

        .box {
            top: 50%;
            left: 50%;
            text-align: center;
            margin-top: 13vh;
            box-shadow: -1px 1px 50px 10px rgb(0, 0, 0);
            border-radius: 10px 0px 20px 0px;
            opacity: 65%;
            background-color: black;
        }

        .card-body,
        .card-header {
            color: white;
        }

        .box input[type="text"],
        .box input[type="number"],
        .box input[type="date"] {
            color: white;
            Background-color: #000000;
            font-weight: bold;
        }
    </style>
</body>

</html>
@section('content')
{{-- <div class="container">
    <div class="row justify-content-center">

        <div class="card">
            <div class="card-header">
              <div class="row">
                  <div class="col-md-6">
                      <h4 class="mt-2">Accounts</h4>
                  </div>
                  <div class="col-md-6">
                      <ul class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
                          <li class="nav-item">
                              <a href="" class="nav-link">Accounts</a>
                          </li>
                      </ul>
                  </div>
              </div>
            </div>
            <div class="card-body">
                <div class="row col-8" style="background-color: #030303">
                    <canvas id="myChart" width="400" height="400"></canvas>
                </div>
            </div>
        </div>

    </div>
</div>
</div>
</div> --}}

<div class="row justify-content-center">
    <div class="col-md-9">
        <div class="row">
            {{-- Visitas --}}
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6" >
                                <ul  class="nav nav-pills" id="pills-tab" role="tablist">
                                <div style="margin-left: 0%">
                                    <li>
                                      <label  style="color: #000000">Date Initial</label>
                                      <input type="date" id="dateInitial">

                                    </li>
                                    <li class="nav-item">
                                        <label  style="color: #000000">Date Final</label>
                                        <input type="date" id="dateFinal">
                                    </li>
                                   

                                </div>
                                  <li class="nav-item">
                                    <a  class="nav-link visits" data-toggle="pill" id="for_dates" href="#"
                                    role="tab" aria-controls="pills-profile" aria-selected="false">Search</a>
                                    </li>
                                </ul>

                            </div>
                            <div class="col-md-6">
                                <ul  class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                       <input  type="date" id="forMonthOrYear">
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link visits" data-toggle="pill" id="month" href="#"
                                            role="tab" aria-controls="pills-profile" aria-selected="false">Month</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link visits" data-toggle="pill" id="year" href="#"
                                            role="tab" aria-controls="pills-profile" aria-selected="false">Year</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link visits active" data-toggle="pill" id="lasth_month" href="#"
                                            role="tab" aria-controls="pills-home" aria-selected="true">Lasth Month</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link visits" data-toggle="pill" id="lasth_year" href="#"
                                            role="tab" aria-controls="pills-profile" aria-selected="false">Last year</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas id="myChart" height="72"></canvas>
                    </div>
                </div>
            </div>
            {{-- /Visitas --}}
        </div>

        <div class="row mt-3">
            {{-- Navegador --}}
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="mt-2">Accounts</h4>
                            </div>
                            <div class="col-md-6">
                                <ul class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link browser active" data-toggle="pill" id="accounts_all"
                                            href="#" role="tab" aria-controls="pills-home" aria-selected="true">Accounts</a>
                                    </li>
                                    
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas id="accountChart"  height="72"></canvas>
                    </div>
                </div>
            </div>
            {{-- /accounts --}}

            {{-- category --}}
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col-md-6">
                                <h4 class="mt-2">Categories</h4>
                            </div>
                            <div class="col-md-6">
                                <ul class="nav nav-pills justify-content-end" id="pills-tab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link platform active" data-toggle="pill" id="category_expenses"
                                            href="#" role="tab" aria-controls="pills-home" aria-selected="true">Expenses</a>
                                    </li>
                                   
                                    <li class="nav-item">
                                        <a class="nav-link platform" data-toggle="pill" id="category_income" href="#"
                                            role="tab" aria-controls="pills-contact" aria-selected="false">Income</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <canvas id="category"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('graphics.show')

</div>


@endsection
<script src="https://cdn.jsdelivr.net/npm/chart.js@3.0.2/dist/chart.min.js"></script>
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="/js/graphics.js"></script>
