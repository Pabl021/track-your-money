


<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>

<style>
    
    body{
    background: url(https://savantwealth.com/wp-content/uploads/2020/03/Focus-on-the-Fundamentals-Tile.jpg) no-repeat center center fixed; 
                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;   
  }
  @import url(//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css);
}
@import url(https://fonts.googleapis.com/css?family=Titillium+Web:300);



.fa-2x {
font-size: 2em;
}
.fa {
position: relative;
display: table-cell;
width: 60px;
height: 36px;
text-align: center;
vertical-align: middle;
font-size:20px;
}


.main-menu:hover,nav.main-menu.expanded {
width:280px;
overflow:visible;
}

.main-menu {
background:#87CEFA;
border-right:1px solid #e5e5e5;
position:absolute;
top:0;
bottom:0;
height:100%;
left:0;
width:53px;
overflow:hidden;
-webkit-transition:width .05s linear;
transition:width .05s linear;
-webkit-transform:translateZ(0) scale(1,1);
z-index:1000;
}

.main-menu>ul {
margin:7px 0;
}

.main-menu li {
position:relative;
display:block;
width:250px;
}

.main-menu li>a {
position:relative;
display:table;
border-collapse:collapse;
border-spacing:0;
color:black;
 font-family: arial;
font-size: 14px;
text-decoration:none;
-webkit-transform:translateZ(0) scale(1,1);
-webkit-transition:all .1s linear;
transition:all .1s linear;
  
}

.main-menu .nav-icon {
position:relative;
display:table-cell;
width:60px;
height:36px;
text-align:center;
vertical-align:middle;
font-size:18px;
}

.main-menu .nav-text {
position:relative;
display:table-cell;
vertical-align:middle;
width:190px;
  font-family: 'Titillium Web', sans-serif;
}

.main-menu>ul.logout {
position:absolute;
left:0;
bottom:0;
}

.no-touch .scrollable.hover {
overflow-y:hidden;
}

.no-touch .scrollable.hover:hover {
overflow-y:auto;
overflow:visible;
}

a:hover,a:focus {
text-decoration:none;
}

nav {
-webkit-user-select:none;
-moz-user-select:none;
-ms-user-select:none;
-o-user-select:none;
user-select:none;
}

nav ul,nav li {
outline:0;
margin:0;
padding:0;
}
.main-menu li:hover>a,nav.main-menu li.active>a,.dropdown-menu>li>a:hover,.dropdown-menu>li>a:focus,.dropdown-menu>.active>a,.dropdown-menu>.active>a:hover,.dropdown-menu>.active>a:focus,.no-touch .dashboard-page nav.dashboard-menu ul li:hover a,.dashboard-page nav.dashboard-menu ul li.active a {
color:black;
background-color:#5fa2db;
}
.area {
float: left;
background: #e2e2e2;
width: 100%;
height: 100%;
}
@font-face {
  font-family: 'Titillium Web';
  font-style: normal;
  font-weight: 300;
  src: local('Titillium WebLight'), local('TitilliumWeb-Light'), url(http://themes.googleusercontent.com/static/fonts/titilliumweb/v2/anMUvcNT0H1YN4FII8wpr24bNCNEoFTpS2BTjF6FB5E.woff) format('woff');
}
  </style>

    <div id="app">
    <nav class="main-menu">
            <ul>
                <li>
                <span style="color:black;"><img src="https://image.flaticon.com/icons/png/128/564/564612.png" alt="Stickman" width="50" height="50">Track Your Money</span>

                  
                </li>
                <li class="has-subnav" >
                    <a href="{{ route('defineLocalCurrency') }}">
                        <i class="fa fa-laptop fa-2x"></i>
                        <span class="nav-text">
                        <span><img src="https://image.flaticon.com/icons/png/128/1611/1611179.png" alt="Stickman" width="30" height="30"></span>
                        <strong> Define local currency </strong>
                        </span>
                    </a>
                    
                </li>
                <li class="has-subnav">
                    <a href=" {{ route('accounts') }}">
                       <i class="fa fa-list fa-2x"></i>
                        <span class="nav-text">
                        <span><img src="https://image.flaticon.com/icons/png/128/633/633611.png" alt="Stickman" width="30" height="30"></span>
                        <strong> My Account </strong>
                        </span>
                    </a>                   
                </li>
                

                <li class="has-subnav">
                    <a href=" {{ route('category') }}">
                       <i class="fa fa-folder-open fa-2x"></i>
                        <span class="nav-text">
                        <span><img src="https://image.flaticon.com/icons/png/128/2780/2780338.png" alt="Stickman" width="30" height="30"></span>
                        <strong>Expense/income category</strong>
                        </span>
                    </a>
                   
                </li>
                <li>

                    <a href="{{route('transactions')}}">

                        <i class="fa fa-bar-chart-o fa-2x"></i>
                        <span class="nav-text">
                        <span><img src="https://image.flaticon.com/icons/png/128/3029/3029336.png" alt="Stickman" width="30" height="30"></span>
                        <strong>Transaction register</strong>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="{{route('graphics')}}">
                        <i class="fa fa-font fa-2x"></i>
                        <span class="nav-text">
                        <span><img src="https://image.flaticon.com/icons/png/128/2198/2198366.png" alt="Stickman" width="30" height="30"></span>
                        <strong> Reports and graphs</strong>
                        </span>
                        
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-font fa-2x"></i>
                        <span class="nav-text">
                        <span><img src="https://image.flaticon.com/icons/png/128/558/558336.png" alt="Stickman" width="30" height="30"></span>
                        Balance per account 
                        </span>
                        
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-font fa-2x"></i>
                        <span class="nav-text">
                        <span><img src="https://image.flaticon.com/icons/png/128/2915/2915664.png" alt="Stickman" width="30" height="30"></span>
                        Detail of expense/income
                        </span>
                        
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class="fa fa-font fa-2x"></i>
                        <span class="nav-text">
                        <span><img src="https://image.flaticon.com/icons/png/128/1153/1153269.png" alt="Stickman" width="30" height="30"></span>
                        Expenses/income by category 
                        </span>
                        
                    </a>
                </li>
                
                
            </ul>
            
            
        </nav>

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>







 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/css/bootstrap.min.css"
    integrity="sha384-Smlep5jCw/wG7hdkwQ/Z5nLIefveQRIY9nfy6xoR1uRYBtpZgI6339F5dgvm/e9B" crossorigin="anonymous">

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script> 

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>


<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js"
    integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em"
    crossorigin="anonymous"></script>

 
