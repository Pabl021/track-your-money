<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

//Coins
Route::get('/defineLocalCurrency', 'DefineLocalCurrencyController@index')->name('defineLocalCurrency');
Route::post('/createCoin', 'DefineLocalCurrencyController@save')->name('save_coin');
Route::delete('/deleteCoin/{id}', 'DefineLocalCurrencyController@delete')->name('deleteCoin');
Route::get('/updateCoin', 'DefineLocalCurrencyController@updateCoin')->name('updateCoin');


Route::get('/redirect', 'Auth\LoginController@redirectToProvider');
Route::get('/callback', 'Auth\LoginController@handleProviderCallback');


Route::get('auth/google', 'Auth\GoogleController@redirectToGoogle');
Route::get('auth/google/callback', 'Auth\GoogleController@handleGoogleCallback');

Route::get('auth/facebook', 'Auth\LoginController@redirectToProviderFace');
Route::get('auth/facebook/callback', 'Auth\LoginController@handleProviderCallback');

//Accounts
Route::get('/accounts', 'AccountController@index')->name('accounts');
Route::get('/account', 'AccountController@getAccount')->name('account');
Route::post('/createAccount', 'AccountController@save')->name('createAccount');
Route::delete('/deleteAccount/{id?}', 'AccountController@delete')->name('deleteAccount'); 
Route::put('/updateAccount/{id?}', 'AccountController@update')->name('updateAccount');
Route::get('/accountFilter', 'AccountController@accountsFilter');

//Transactions
Route::get('/transactions', 'TransactionController@index')->name('transactions');
Route::get('/expensives', 'TransactionController@expensives')->name('expensives');
Route::get('/incomes', 'TransactionController@incomes')->name('incomes');
Route::post('/save', 'TransactionController@save')->name('save');
Route::delete('/deleteTransaction/{id?}', 'TransactionController@delete')->name('deleteTransaction');
Route::get('/updateTransaction/{id?}', 'TransactionController@getTransactionUpdate')->name('updateTransaction');

Route::put('/editTransaction/{id?}', 'TransactionController@update')->name('editTransaction');

Route::get('/allTransactions', 'TransactionController@showAllTransactions')->name('allTransactions');



//graphics
Route::get('/graphics', 'GraphicController@index')->name('graphics');
Route::get('/accountGraphic', 'GraphicController@accounts')->name('accountGraphic');
Route::get('/lastMonth', 'GraphicController@lastMonth')->name('lastMonth');
Route::get('/lastYear', 'GraphicController@lastYear')->name('lastYear');
Route::get('/forDates', 'GraphicController@forDates')->name('forDates');
Route::get('/forMonth', 'GraphicController@forMonth')->name('forMonth');
Route::get('/forYear', 'GraphicController@forYear')->name('forYear');
Route::get('/forExpensesCategory', 'GraphicController@expensesForCategory')->name('forExpensesCategory');
Route::get('/getCategoryGraph/{category?}', 'GraphicController@getCategory')->name('getCategoryGraph');

// Route::get('/getSubCategoryGraph', 'GraphicController@getSubCategories')->name('getSubCategoryGraph');


//Route::get('/', 'GraphicController@getCategory')->name('getCategoryGraph');



// Route::get('/forIncomeCategory', 'GraphicController@incomeForCategory')->name('forIncomeCategory');













Route::get('/createUser', 'CoinController@user');
Route::get('/createType', 'CoinController@type');
Route::get('/createCategory', 'CoinController@category');
Route::get('/createSubCategory', 'CoinController@sub_category');
Route::get('/createTransaction', 'CoinController@transaction');
Route::get('/query', 'CoinController@query');

Route::get('/dashboard', 'CoinController@index');
Route::get('/type', 'CoinController@getTypes');
// Route::get('/accounts', 'CoinController@getAccounts');
// Route::get('/transactions', 'CoinController@getTransactions');




Route::get('/myCoins', 'CoinController@getCoins');
Route::put('/updateCoin/{id?}', 'DefineLocalCurrencyController@update')->name('update');

//Expense Income Category
Route::get('/category','ExpenseIncomeCategoryController@index')->name('category');
Route::post('/insertCategory', 'ExpenseIncomeCategoryController@insertCategory')->name('insertCategory');
Route::delete('/deleteCategory/{id?}', 'ExpenseIncomeCategoryController@deleteCategory')->name('deleteCategory');
Route::get('/getCategory', 'ExpenseIncomeCategoryController@getCategory')->name('getCategory');
Route::put('/updateCategory/{id?}', 'ExpenseIncomeCategoryController@update')->name('updateCategory');



Route::post('/insertSubCategory', 'ExpenseIncomeCategoryController@insertSubCategory')->name('insertSubCategory');
Route::delete('/deleteSubCategory/{id?}', 'ExpenseIncomeCategoryController@deleteSubCategory')->name('deleteSubCategory');
Route::get('/getSubCategory', 'ExpenseIncomeCategoryController@getSubCategory')->name('getSubCategory');
Route::put('/updateSubCategory/{id?}', 'ExpenseIncomeCategoryController@updateSubCat')->name('updateSubCategory');


Route::get('/InfoCategory', 'ExpenseIncomeCategoryController@indexCategory')->name('indexCategory');

//TransactionRegister
Route::get('/transactionRegister', 'TransactionController@index')->name('transaction');
