$(function () {
    var lastMonthCtx = document.getElementById('myChart').getContext('2d');
    var lastMonthChart = new Chart(lastMonthCtx);

    var accountCtx = document.getElementById('accountChart').getContext('2d');
    var accountChart = new Chart(accountCtx);

    
    var categoryCtx = document.getElementById('category').getContext('2d');
    var categoryChart = new Chart(categoryCtx);

    $(document).on("click", "#lasth_year", function () {
        console.log("Hola clickeando ultimo año");
        loadExpensivesIncomesLastYear();
    });

    

    $(document).on("click", "#lasth_month", function () {
        console.log("Hola clickeando ultimo mes");
        loadExpensivesIncomes();
    });

    $(document).on("click", "#accounts_all", function () {
        console.log("Hola clickeando cuentas");
        loadAccounts(); 
    });

    $(document).on("click", "#for_dates", function () {
        var firstDate = document.getElementById('dateInitial').value; 
        var lastDate = document.getElementById('dateFinal').value; 
        if(!firstDate){
            alert("You didn't select the starting date!"); 
        }else if(!lastDate){
            alert("You didn't select the end date!"); 
        }else{
            console.log("Fecha inicial: " + firstDate 
            + "  fecha ultima: " + lastDate);
    
    
            console.log("Hola clickeando dates");
            loadForDate(firstDate, lastDate); 
        }
    });

    $(document).on("click", "#month", function () {
        var dateFY = document.getElementById('forMonthOrYear').value; 
        if(!dateFY){
            alert("You must select a month or year!"); 
        }else{
           loadForMonthOrYear(dateFY, 'forMonth'); 
        }

    });


    $(document).on("click", "#year", function () {
        var dateFY = document.getElementById('forMonthOrYear').value; 
        if(!dateFY){
            alert("You must select a month or year!"); 
        }else{
           loadForMonthOrYear(dateFY, 'forYear'); 
        }

    });

    $(document).on("click", "#category_expenses", function () {
        console.log("Hola clickeando ultimo categories");
        loadCategoriesExpensesIncome('forExpensesCategory', 'Expensive'); 
    });

    $(document).on("click", "#category_income", function () {
        console.log("Hola clickeando ultimo categories");
        loadCategoriesExpensesIncome('forExpensesCategory', 'Income'); 
    });

    function loadCategoriesExpensesIncome(url, isType){
        $.ajax({
            type: "get",
            url: url,
            data: { isType }, 

            //dataType: 'json', 
            success: function (data) {
                valuesAll = [];
                labels = [];
                amountCategory =0; 
                data = JSON.parse(data);
                data.forEach(e => {
                    e.transactions.forEach(t => {
                        amountCategory = amountCategory +t.amount;
                    });
                    valuesAll.push(amountCategory);
                    labels.push(e.description); 
                    amountCategory = 0; 
                });
                console.log(data);
                loadCategoriesChart(valuesAll, labels); 
            }
        });
    }

    // function loadCategoriesIncome(){
    //     $.ajax({
    //         type: "get",
    //         url: 'forIncomeCategory',
    //         //dataType: 'json', 
    //         success: function (data) {
    //             valuesAll = [];
    //             labels = [];
    //             amountCategory =0; 
    //             data = JSON.parse(data);
    //             data.forEach(e => {
    //                 e.transactions.forEach(t => {
    //                     amountCategory = amountCategory +t.amount;
    //                 });
    //                 valuesAll.push(amountCategory);
    //                 labels.push(e.description); 
    //                 amountCategory = 0; 
    //             });
    //             console.log(data);
    //             loadCategoriesChart(valuesAll, labels); 
    //         }
    //     });
    // }

    function loadForMonthOrYear(dateMonthOrYear, url){
        $.ajax({
            type: "get",
            url: url,
            data: { dateMonthOrYear }, 

            //dataType: 'json', 
            success: function (data) {

                nameMonth = getNameMonth(dateMonthOrYear); 
                msjExpenses = 'Expenses for the month of '+ nameMonth; 
                msjIncome = 'Income for the month of '+ nameMonth; 

                if(url==='forYear'){
                    nameMonth = new Date(dateMonthOrYear).getFullYear(); 
                    msjExpenses = 'Expenses for year ' +nameMonth; 
                    msjIncome = 'Income for year ' +nameMonth; 

                }

                valuesAll = [];
                labels = [msjExpenses, msjIncome];
                data = JSON.parse(data);
                
                valuesAll.push(data.transactionExpensive)
                valuesAll.push(data.transactionIncome)
                loadGraph(valuesAll, labels);
            },
            error: function(request, status, error){
                console.log(request.responseText); 
            }
        });
    }

    function loadForDate(dateInitial, dateFinal){
        $.ajax({
            type: "get",
            url: 'forDates',
            data: { dateInitial, dateFinal }, 

            //dataType: 'json', 
            success: function (data) {
                valuesAll = [];
                labels = ['Expenses '+ dateInitial, 'Income for '+dateFinal];
                data = JSON.parse(data);
                valuesAll.push(data.transactionExpensive)
                valuesAll.push(data.transactionIncome)
                loadGraph(valuesAll, labels);
            }
        });
    }


    function loadExpensivesIncomesLastYear() {
        $.ajax({
            type: "get",
            url: 'lastYear',
            //dataType: 'json', 
            success: function (data) {
                valuesAll = [];
                labels = ['Expenses for year', 'Income for year'];
                data = JSON.parse(data);
                valuesAll.push(data.lastYearExpensive)
                valuesAll.push(data.lastYearIncome)
                console.log(valuesAll);

                loadGraph(valuesAll, labels);
            }
        });
    }

    function loadExpensivesIncomes() {
        $.ajax({
            type: "get",
            url: 'lastMonth',
            //dataType: 'json', 
            success: function (data) {
                valuesAll = [];
                labels = ['Expenses for month', 'Income for month'];
                data = JSON.parse(data);
                valuesAll.push(data.lastMonthExpensive)
                valuesAll.push(data.lastMonthIncome)
                loadGraph(valuesAll, labels);
            }
        });
    }

    function loadAccounts() {
        $.ajax({
            type: "get",
            url: 'accountGraphic',
            //dataType: 'json', 
            success: function (data) {
                valuesAll = [];
                labels = [];
               
                // console.log(valuesAll);
                data = JSON.parse(data);
                data.forEach(e => {
                    valuesAll.push(e.monthly_budget)
                    labels.push(e.name)
                });
                loadAccountChart(valuesAll, labels)
            }
        });
    }

    function getNameMonth(date){
        var months = new Array();
        months[0] = "January";
        months[1] = "February";
        months[2] = "March";
        months[3] = "April";
        months[4] = "May";
        months[5] = "June";
        months[6] = "July";
        months[7] = "August";
        months[8] = "September";
        months[9] = "October";
        months[10] = "November";
        months[11] = "December";

        var month = months[new Date(date).getMonth()];
        return month; 
    }

    function loadGraph(valuesAll, labels) {
        lastMonthChart.destroy();
        //  var lastMonthCtx = document.getElementById('myChart').getContext('2d');
        lastMonthChart = new Chart(lastMonthCtx, {
            type: 'bar',
            data: {
                labels: labels,
                datasets: [{
                    label: '• Sum of expenses and income',
                    data: valuesAll,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }

    function loadAccountChart(data, labels) {
        accountChart.destroy();
        accountChart = new Chart(accountCtx, {
            type: 'doughnut',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Accounts',
                    data: data,
                    borderWidth: 1,
                    backgroundColor: ['#dc3545', '#ffc107', '#17a2b8', '#28a745', '#666666', '#000000']
                }],
            },
            options: {
                legend: {
                    position: 'bottom'
                },
            }
        });

    }

    function loadCategoriesChart(data, labels) {
        categoryChart.destroy();
        categoryChart = new Chart(categoryCtx, {
            type: 'pie',
            data: {
                labels: labels,
                datasets: [{
                    label: 'Categories',
                    data: data,
                    borderWidth: 1,
                    backgroundColor: ['#dc3545', '#ffc107', '#17a2b8', '#28a745', '#666666', '#000000']
                }],
            },
            options: {
                plugins: {
                    legend: {
                        display: true,
                        onClick:  getTransactionsForCategory,
                        // position: 'bottom',

                    }
                },
                // legend: {
                //     display:true, 
                //     onClick: showTransactions,
                //     position: 'bottom',
                // },
            }
        });

    }

    function getTransactionsForCategory(ev, legendItem){
        var category = legendItem.text;
        // console.log(category); 
        // $.ajax({
        //     type: "get",
        //     url: 'getCategoryGraph',
        //     data: { category }, 

        //     //dataType: 'json', 
        //     success: function (data) {
        //         valuesAll = [];
        //         labels = [];
        //     },
        //     error: function(request, status, error){
        //         console.log(request.responseText); 
        //     }
      //  });
       // showTransactions(category); 
        window.location.href='http://proyecto2.com/getCategoryGraph/'+category;

    }

    function showTransactions(category){
      //  $("#cateTest").val('Este es el nuevo nombre');
       // $('#btnModalTransactions').modal('show'); 
    }

    loadExpensivesIncomes();
    loadAccounts(); 
    loadCategoriesExpensesIncome('forExpensesCategory', 'Expensive'); 

});











