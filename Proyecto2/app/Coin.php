<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    public function user(){
        return $this->belongsTo(User::class); 
    }

    public function accounts(){
        return $this->hasMany(Account::class);
    }
}
