<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function account(){
        return $this->belongsTo(Account::class); 
    }

    public function type(){
        return $this->belongsTo(Type::class); 
    }
    
    
    public function category(){
        return $this->belongsTo(Category::class); 
    }
}
