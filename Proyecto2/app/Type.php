<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    public function categories(){
        return $this->hasMany(Category::class);
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }
}
