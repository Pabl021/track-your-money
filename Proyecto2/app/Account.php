<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function coin(){
        return $this->belongsTo(Coin::class); 
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }
}
