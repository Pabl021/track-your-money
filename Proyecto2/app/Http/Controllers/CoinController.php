<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App; 

class CoinController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index(){
        return view('dashboard'); 
    }

    public function type (){
        $t = new App\Type; 
        $t->name ="Expensive"; 
        $t->save(); 

        $t1 = new App\Type; 
        $t1->name ="Income"; 
        $t1->save(); 

        $t2 = new App\Type; 
        $t2->name ="Transfer"; 
        $t2->save(); 
        echo "Saved Type"; 
    }

    public function category (){
        $c = new App\Category; 
        $c->type_id =3; 
        $c->description ="Transfer";
        $c->monthly_budget =0;
        $c->save(); 
        echo "Saved Category"; 
    }

    public function sub_category (){
        $c = new App\SubCategory; 
        $c->category_id =2; 
        $c->description ="Trabajando como desarrollador desktop";
        $c->monthly_budget =200000;
        $c->save(); 
        echo "Saved Sub Category"; 
    }

    public function transaction (){
        $c = new App\Transaction; 
        $c->account_id =1; 
        $c->type_id =2;
        $c->category_id =1;
        $c->amount =20000; 
        $c->detail ="Arreglo en taller santa rosa";
        $c->save(); 
        echo "Saved Transaction"; 
    }

    public function getTypes (){
        $types = App\Type::findOrFail(1);
        return view('dashboard', compact('types')); 
    }

    public function getAccounts(){
        $user = App\User::findOrFail(1); 
        $accounts = $user->accounts; 
        return view('dashboard', compact('accounts')); 
    }

    public function getTransactions(){
        $transaction = App\Transaction::findOrFail(1); 
        return view('dashboard', compact('transaction')); 
    }

    public function getCoins(){       
        $user = App\User::findOrFail(1); 
        return view('dashboard', compact('user'));
    }
}
