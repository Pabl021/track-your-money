<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use Illuminate\Support\Facades\Auth;

class ExpenseIncomeCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user = App\User::findOrFail(auth()->user()->id);    
        $categories = $user->categories; //esto obtiene solo las categorias del usuario

        foreach ($categories as $c) {
            $c->subcategories; 
        }

        $type = \DB::table('types')->select('types.*')->take(2)->get();
     //   $categories = \DB::table('categories')->select('categories.*')->get(); esto carga toda las categorias
     //incluyendo las que no son del usuario
     //   $subCategories = \DB::table('sub_categories')->select('sub_categories.*')->get();
        //esta cargan las subcategorias todas
        return view('Expense_income_category.index')
        ->with('types', $type)
        ->with('categories', $categories);
     //   ->with('subCategories', $subCategories);
    }

    public function indexCategory(){

        $user = App\User::findOrFail(auth()->user()->id);    
        $categories = $user->categories; //esto obtiene solo las categorias del usuario

        foreach ($categories as $c) {
            $c->subcategories; 
        }

        $type = \DB::table('types')->select('types.*')->take(2)->get();
        return view('Expense_income_category.infoCategory')
        ->with('types', $type)
        ->with('categories', $categories);

    }

    public function insertCategory(){
        $Category = new App\Category;
        $Category->user_id = Auth()->user()->id; 
        $Category->type_id = request("type"); 
        $Category->description=request('categoryName'); 
        $Category->monthly_budget=request('monthly'); 
        $Category->save(); 
        return back(); 
    }

    public function insertSubCategory(){
        $subCategory = new App\SubCategory;
       
        $subCategory->category_id = request("typeCat"); 
        $subCategory->description=request('description'); 
        $subCategory->monthly_budget=request('monthlySubCat'); 
        $subCategory->save(); 
        return back(); 
    }

    public function update($id){
        $Category = App\Category::findOrFail($id);
        $Category->user_id = Auth()->user()->id; 
        $Category->type_id = request("typeCatUpdate"); 
        $Category->description=request('descriptionUpdate'); 
        $Category->monthly_budget=request('monthlyCatUpdate'); 
        $Category->save(); 
        return back(); 
    }

    public function updateSubCat($id){
        $subCategory = App\SubCategory::findOrFail($id);
        $subCategory->category_id = request("typeSubUpdate"); 
        $subCategory->description=request('descriptionSubUpdate'); 
        $subCategory->monthly_budget=request('monthlySubCatUpdate'); 
        $subCategory->save(); 
        return back(); 
    }


    public function deleteCategory($id)
    {
        $deleteCategory = App\Category::FindOrFail($id);
        $deleteCategory->delete();
        return back(); 
    }

    public function deleteSubCategory($id)
    {
        $deleteSubCategory = App\SubCategory::FindOrFail($id);
        $deleteSubCategory->delete();
        return back(); 
    }

    public function getCategory(Request $r)
    {
        $categ = App\Category::findOrFail($r->id); 
        $categ = json_encode($categ);
        return $categ;
    }

    public function getSubCategory(Request $request)
    {
        $subCate = App\SubCategory::findOrFail($request->id); 
        $subCate = json_encode($subCate);
        return $subCate;
    }

}
