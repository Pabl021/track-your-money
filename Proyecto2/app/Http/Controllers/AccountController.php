<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use App; 


class AccountController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $user = App\User::findOrFail(auth()->user()->id);
        if(count($user->coins)<1){
            $msjError = 'You dont have coin';
            return view('errors.errorAccount', compact('msjError'));
        } 
        $accounts = App\Account::where('user_id', auth()->user()->id)->paginate(6);
        return view('accounts.index',compact('accounts', 'user'));
    }

    public function getAccount(Request $r)
    {
        $user = App\Account::findOrFail($r->id); 
        $user = json_encode($user);
        return $user;
    }

    public function save (){
        $this->validateAccount();
        $a = new App\Account; 
        $a->user_id =auth()->user()->id; 
        $a->coin_id =request('coin'); 
        $a->name =request('name');
        $a->description =request('description');
        $a->monthly_budget =request('monthly');
        $a->save(); 
        return back(); 
    }

    
    public function delete($id){
        $a = App\Account::findOrFail($id);
        $a->delete();
        return back(); 
    }

    public function update($id){
        $this->validateAccount(); 
        $a = App\Account::findOrFail($id);
        $a->user_id =auth()->user()->id; 
        $a->coin_id =request('coin'); 
        $a->name =request('name');
        $a->description =request('description');
        $a->monthly_budget =request('monthly');
        $a->save();  
        return back(); 

    }

    public function validateAccount(){
        return request()->validate([
            'name'=>'required',
            'description'=>'required',
            'monthly'=>'required',
            'coin'=>'exists:coins,id'
        ]);
    }

    public function accountsFilter(Request $request)
    {
        $accounts = App\Account::where('name', 'LIKE','%' . $request->text . '%')->paginate(6);
        return view('accounts.show', compact('accounts'));
    }
}
