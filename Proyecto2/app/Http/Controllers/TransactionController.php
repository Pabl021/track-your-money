<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

use Exception; 

use App; 


class TransactionController extends Controller
{

    protected $accountInitial;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(){
        $user = App\User::findOrFail(Auth()->user()->id); 

        $CoinLocal = App\Coin::where('user_id',auth()->user()->id)->where('is_local',true)->first(); 
        if(!$CoinLocal){
            $msjError = 'You dont have currency local';
            return view('errors.showErrors', compact('msjError')); 
        }
        $accountWithCoinLocal = $CoinLocal-> accounts; 
        if(count($accountWithCoinLocal)<1){
            $msjError = 'You dont have accounts';
            return view('errors.showErrors', compact('msjError'));
        }
        $transaction = App\Type::where('name', 'Transfer')->first();
        return view('transactions.index', compact('transaction', 'user', 'accountWithCoinLocal')); 
    }

    public function expensives(){
        $user = App\User::findOrFail(Auth()->user()->id); 
        if(count($user->accounts)<1){
            $msjError = 'You dont have accounts';
            return view('errors.showErrors', compact('msjError'));
        }

        $expensive = App\Type::where('name', 'Expensive')->first();
        $listExpensive =[]; 

        foreach ($expensive->categories as $c) {
            if($c->user_id==$user->id){
                array_push($listExpensive, $c);
            }
        }
        $expensive->categories = $listExpensive; 
        if(count($expensive->categories)<1){
            $msjError = 'You dont have categories by expensive';
            return view('errors.showErrors', compact('msjError'));
        }
        return view('transactions.index', compact('expensive', 'user')); 
    }

    public function incomes(){
        $user = App\User::findOrFail(Auth()->user()->id);
        if(count($user->accounts)<1){
            $msjError = 'You dont have accounts';
            return view('errors.showErrors', compact('msjError'));
        }
        $income = App\Type::where('name', 'Income')->first();
        $listIncome =[]; 

        foreach ($income->categories as $c) {
            if($c->user_id==$user->id){
                array_push($listIncome, $c);
            }
        }
        $income->categories = $listIncome; 
        if(count($income->categories)<1){
            $msjError = 'You dont have categories by income';
            return view('errors.showErrors', compact('msjError'));
        }
        return view('transactions.index', compact('income', 'user')); 
    }

    public function save(){
        //$this->validateTransaction(); 
        try {
            $t = $this->newTransaction(); 
            if(request('isType') === 'isTransaction'){
                $isType = 'isExpensive'; 
                $t->type_id =$this->getType('Expensive')->id; 
                $t->category_id = $this->getCategory('Transfer', $t->type_id)->id;
                for ($i=0; $i < 2; $i++) { 
                   $this->changeAmount($isType, $t); 
                    $t = $this->newTransaction(); 
                    $t->account_id = request('accountDeposit'); 
                  //  $t->account_id = request('accounts'); 
                    $t->type_id =$this->getType('Income')->id; 
                    $t->category_id = $this->getCategory('Transfer', $t->type_id)->id;
                    $isType = 'isIncome';
                }
                return back(); 
            } 
            $this->changeAmount(request('isType'), $t); 
            return back(); 
        } catch (\Exception $ex) {
            $error = $ex->getMessage();
            return back()->with('error', $error); 
        }
    }

    public function update($id){
        try {
            $transactionUpdate = App\Transaction::findOrFail($id);
            $amountCurrent = $transactionUpdate->amount; 
            $accountCurrent =  $transactionUpdate->account; 
            $transactionUpdate->user_id = Auth()->user()->id; 
            $transactionUpdate->account_id = request('accounts'); 
            $transactionUpdate->type_id = request('type');
            $transactionUpdate->category_id = request('category');
            $transactionUpdate->detail = request('detail');
            $transactionUpdate->amount = request('amount');
            $transactionUpdate->date_transaction = request('dateTransaction');

            // if(request('isType') === 'isTransaction'){
            //     for ($i=0; $i < 2; $i++) {
            //         $this->validateTransactionUpdate($transactionUpdate, $amountCurrent,  $accountCurrent); 
            //     }
            //     return back(); 
            // } 
            $this->validateTransactionUpdate($transactionUpdate, $amountCurrent,  $accountCurrent); 
            return back(); 
        } catch (\Exception $ex) {
            $error = $ex->getMessage();
            return back()->with('error', $error); 
        }
    }

    public function validateTransactionUpdate($transaction, $amountCurrent, $accountCurrent){
        //1200 new
        //2 old

        //2000 old ->1998
        //1500 new -> 1200 ? 2

        //2000 old -> 1200 ? 2

        //expensive 1998 +2 reduce 1200 = 800
        //income 2002 -2 add 1200 = 3200

        //expensive 100 +2 reduce 1200 = 302
        //income 1500 -2 add 1200 = 2698

        if($amountCurrent < 0){
            $transaction->amount = $transaction->amount*-1; 
        }
        if($transaction->amount != $amountCurrent || $accountCurrent->id!=$transaction->account_id){
            if($transaction->type->name==='Expensive'){
                $accountCurrent->monthly_budget =  $accountCurrent->monthly_budget+ (-1*$amountCurrent);
            }else if($transaction->type->name==='Income'){
                $accountCurrent->monthly_budget = $accountCurrent->monthly_budget-($amountCurrent);
            }
            $transaction->amount = $transaction->amount*-1; 
            if($transaction->type->name==='Income'){
                $transaction->amount = $transaction->amount*-1; 
            }
            $this->validateAmount($accountCurrent, $transaction);
            $accountCurrent->save(); 
            $this->changeAmount(request('isType'), $transaction); 
        }else{
            $transaction->update(); 
        }
    }

    public function validateRefund($account, $newAmount, $type){
            //12                    //12000
        if($account->monthly_budget < $newAmount && $type=='Income'){
            //gasto +sumarlos
            //ingreso -quitarlos
            throw new Exception(''); 
        }
    }

    public function changeAmountTransaction($transaction, $accountCurrent){
        $account = App\Account::findOrFail($transaction->account_id); 
        if($accountCurrent->coin_id !=$account->coin_id){
            $transaction->amount = $account->coin->rate * $transaction->amount;
            dd($transaction->amount); 
        }
    }

    public function changeAmount($isType, $transaction){
        $account = App\Account::findOrFail($transaction->account_id);
        $this->convertCoin($account, $transaction); 

        $this->validateAmount($account, $transaction); 

        $amountAccount  =  $account->monthly_budget; 
        if($isType ==='isExpensive'){
           $account->monthly_budget =$amountAccount - $transaction->amount;
           $account->save(); 
           $transaction->amount = $transaction->amount*-1;
           $transaction->save(); 
           return;  
        }
        $account->monthly_budget = $amountAccount + $transaction->amount;
        $account->save(); 
        $transaction->save(); 
        return;
    }

    public function newTransaction(){
        $t = new App\Transaction;
        $t->user_id = Auth()->user()->id; 
        $t->account_id = request('accounts'); 
        $t->type_id = request('type');
        $t->category_id = request('category');
        $t->detail = request('detail');
        $t->amount = request('amount');
        $t->date_transaction = request('dateTransaction');
        return $t; 
    }

    public function validateTransaction(){
        return request()->validate([
            'amount'=>'required',
            'detail'=>'required',
            // 'coin'=>'exists:coins,id'
        ]);
    }
 
    public function convertCoin($account, $transaction){
        if($this->accountInitial != null){
            if($account->coin_id != $this->accountInitial->coin_id){
                    $transaction->amount = $transaction->amount * $this->accountInitial->coin->rate; 
            }
            $this->accountInitial = null;
        }
        $this->accountInitial = $account; 
    }

    public function validateAmount($account, $transaction){
        if($transaction->amount<0){
            throw new Exception('You should write numbers greater than 0');
        }
        
        if($transaction->amount > $account->monthly_budget  &&$transaction->type->name=='Expensive' ){
            throw new Exception('The given amount of the transfer is less than the account amount'); 
        }
    }

    public function getType($name){
        if($name==='Income'){
            $income = App\Type::where('name', $name)->first();
            return $income; 
        }
        $expensive = App\Type::where('name', $name)->first();
        return $expensive; 
    }

    public function getCategory($name, $type_id){
        $category = App\Category::where('description', $name)->where('type_id',$type_id)->first();
        return $category; 
    }

    public function showAllTransactions(){
        $allTransactions = App\Transaction::where('user_id', auth()->user()->id)->paginate(5);

        // $user = App\User::findOrFail(Auth()->user()->id); 
        // $allTransactions = $user->transactions;
        return view('transactions.show', compact('allTransactions')); 
    }
    public function delete($id){
        $a = App\Transaction::findOrFail($id);
        $account = $a->account; 
        if($a->type->name == 'Expensive'){
            $result = $a->amount*-1; 
            $account->monthly_budget =  $account->monthly_budget+$result; 
        }
        if($a->type->name == 'Income'){
            $result =$account->monthly_budget - $a->amount;
            $account->monthly_budget = $result < 0 ? (($result*-1)+$result): ($result == 0 ? 0: $result); 
        }
        $account->save(); 
        $a->delete();
        return back(); 
    }

    public function getTransactionUpdate($id){
        $transactionUpdate = App\Transaction::findOrFail($id);
        $type =  App\Type::where('name', 'Income')->first();
        if($transactionUpdate->type->name == 'Expensive'){
            $type = App\Type::where('name', 'Expensive')->first();
            $transactionUpdate->amount= $transactionUpdate->amount*-1; 
        }
        $user = App\User::findOrFail(Auth()->user()->id); 
        if($transactionUpdate->category->description === 'Transfer'){
            $CoinLocal = App\Coin::where('user_id',auth()->user()->id)->where('is_local',true)->first(); 
            $accountWithCoinLocal = $CoinLocal-> accounts; 
            return view('transactions.edit', compact('transactionUpdate', 'user','accountWithCoinLocal')); 
        }
        return view('transactions.edit', compact('transactionUpdate', 'user', 'type')); 
    }

}
