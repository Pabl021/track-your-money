<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon; 
use App; 

class GraphicController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    //
    public function index()
    {
        return view('graphics.index');
    }

    public function accounts(){
        $user = App\User::findOrFail(auth()->user()->id); 
        $accounts = $user->accounts;
        foreach ($accounts as $a) {
           $a->monthly_budget = $a->coin->rate*$a->monthly_budget; 
        } 
        $accounts = json_encode($accounts); 
        return $accounts; 
    }

    public function lastMonth(){
        $dateNow =Carbon::now()->subDays(30)->format('Y-m-d'); 

        // $lastMonthExpensive=App\Transaction::where('user_id', auth()->user()->id)
        // ->where('type_id', '!=',3)
        // ->whereDate('date_transaction','>=', $dateNow)->get();

        $lastMonthExpensive=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id',1)
        ->whereDate('date_transaction','>=', $dateNow)->get(); 

        $lastMonthExpensive = $this->convertCoinToLocal($lastMonthExpensive); 

        $lastMonthIncome=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id',2)
        ->whereDate('date_transaction','>=', $dateNow)->get();

        $lastMonthIncome = $this->convertCoinToLocal($lastMonthIncome); 


        // foreach ($lastMonthExpensive as $t) {
        //     $t->category;
        //     $t->type;
        //     $coin = $t->account->coin;
        // //    echo "Es  ".($coin->is_local?"Moneda local":"Moneda extranjera")."<br>";
        // //    echo "Monto rate: ".$coin->rate."<br>";
        // //    echo "Monto cuenta: ".$t->account->monthly_budget."<br>";
        // //    echo "Monto valor igual: ".(($t->amount*-1)*$coin->rate)."<br>";
        // //    echo "Monto transacion: ".$t->amount."<br><br>";
        // if($t->amount<0){
        //    $t->amount = ($t->amount*-1)*$coin->rate; 
        // }

        // }
        $dataValues=  ['lastMonthExpensive'=>$lastMonthExpensive, 'lastMonthIncome'=>$lastMonthIncome]; 
        $dataValues= json_encode($dataValues); 
        return $dataValues; 

    }


    public function lastYear(){
        $dateNow =Carbon::now()->subYears(1)->format('Y-m-d'); 

        $lastYearExpensive=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id',1)
        ->whereDate('date_transaction','>=', $dateNow)->get();
        $lastYearExpensive = $this->convertCoinToLocal($lastYearExpensive); 
        

        $lastYearIncome=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id',2)
        ->whereDate('date_transaction','>=', $dateNow)->get();
        $lastYearIncome = $this->convertCoinToLocal($lastYearIncome); 


        $dataValues=  ['lastYearExpensive'=>$lastYearExpensive, 'lastYearIncome'=>$lastYearIncome]; 
        $dataValues= json_encode($dataValues); 
        return $dataValues; 
    }

    public function lastMonthIncome(){
        $dateNow =Carbon::now()->subDays(30)->format('Y-m-d'); 

        $lastMonthIncome=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id', 2)
        ->whereDate('date_transaction','>=', $dateNow)->get();
    }

    public function forMonth(Request $r)
    {
        $month =  Carbon::parse($r->dateMonthOrYear)->month;//get date for user

        $tExpensive=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id',1)
        ->whereMonth('date_transaction','=', $month)->get();
        $tExpensive = $this->convertCoinToLocal($tExpensive); 
        

        $tIncome=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id',2)
        ->whereMonth('date_transaction','=', $month)->get();
        $tIncome = $this->convertCoinToLocal($tIncome); 


        $dataValues=  ['transactionExpensive'=>$tExpensive, 'transactionIncome'=>$tIncome]; 
        $dataValues= json_encode($dataValues); 
        return $dataValues; 
     
    }


    public function forYear(Request $r)
    {
        $year = Carbon::parse($r->dateMonthOrYear)->year;//get date for user

        $tExpensive=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id',1)
        ->whereYear('date_transaction','=', $year)->get();
        $tExpensive = $this->convertCoinToLocal($tExpensive); 
        

        $tIncome=App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id',2)
        ->whereYear('date_transaction','=', $year)->get();
        $tIncome = $this->convertCoinToLocal($tIncome); 


        $dataValues=  ['transactionExpensive'=>$tExpensive, 'transactionIncome'=>$tIncome]; 
        $dataValues= json_encode($dataValues); 
        return $dataValues; 
     
    }

    public function forDates(Request $r){
       
        $transactionsExpensive = App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id', 1)
        ->whereBetween('date_transaction', [$r->dateInitial, $r->dateFinal])->get();
        $transactionsExpensive= $this->convertCoinToLocal($transactionsExpensive); 


        $transactionsIncome = App\Transaction::where('user_id', auth()->user()->id)
        ->where('type_id', 2)
        ->whereBetween('date_transaction', [$r->dateInitial, $r->dateFinal])->get();
        $transactionsIncome = $this->convertCoinToLocal($transactionsIncome); 
      
        $dataValues=  ['transactionExpensive'=> $transactionsExpensive, 'transactionIncome'=>$transactionsIncome]; 
        return json_encode($dataValues); 
    }

    public function expensesForCategory(Request $r){


        $expensivesOrIncomeCategory = App\Category::where('type_id', 1)
                    ->where('user_id', auth()->user()->id)->get(); 

        if($r->isType=='Income'){
            $expensivesOrIncomeCategory = App\Category::where('type_id', 2)
            ->where('user_id', auth()->user()->id)->get(); 
        }

        foreach ($expensivesOrIncomeCategory as $c) {

            foreach ($c->transactions as $t) {
                $coin = $t->account->coin; 
                if($t->amount<0){
                    $t->amount= $t->amount*-1;
                }
                $t->amount= ($t->amount*$coin->rate);
          //  echo " Amount convert: ".$t->amount."<br>";

            }
            //echo "Category: " . $c->description."<br>"; 
        }

        return json_encode($expensivesOrIncomeCategory); 
    }


    public function getCategory($category){


        $Category = App\Category::where('description', $category)
        ->where('user_id', auth()->user()->id)->first();

        if(!$Category){
            abort(404);
        }
        $Category->transactions;
        $sub =$Category->subcategories;

        // foreach ($category as $c) {
        //     $c->transactions;
        //     $c->subcategories;

        // }
       // return json_encode($Category); 
       return view('graphics.transactions', compact('Category', 'sub')); 
    }

    public function getSubCategories(){


        // $Category = App\Category::where('id', $r->id)
        // ->where('user_id', auth()->user()->id)->first();

        
      
        // foreach ($category as $c) {
        //     $c->transactions;
        //     $c->subcategories;

        // }
       // return json_encode($Category); 
      // return json_encode($Category->subcategories); 
    }

    public function convertCoinToLocal($listTransactions){
        $total = 0;
        foreach ($listTransactions as $t) {
            $coin = $t->account->coin;
            if($t->amount<0){
                $t->amount= $t->amount*-1;
            }
            $t->amount= ($t->amount*$coin->rate);
            $total = $t->amount + $total; 
        }
        return $total; 
    }
}
