<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App;

class DefineLocalCurrencyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $user = App\User::findOrFail(auth()->user()->id);
        $coins = $user->coins; 
        $message ='You are defining foreign currencies'; 
     //   $coins = \DB::table('coins')->select('coins.*')->get();
        if($coins->isEmpty()){
            $message = 'You don´t have a defined local currency, you are currently defining a local currency'; 
        }
        // return view('Define_local_currency.defineLocalCurrency')->with('coins', $coins);
        return view('Define_local_currency.defineLocalCurrency', compact('coins', 'message'));

    }

    public function save (){
        $this->validateCoin();
        $coin = new App\Coin();
        $user = Auth::user();
        $coin->user_id = $user->id; 
        $coin->name = request('name'); 
        $coin->description = request('description');  
        $coin->rate = request('interestRate');  
        $this->existLocalCurrency($coin); 
        $coin->save(); 
        return back(); 
    }

    public function delete($id)
    {
        $deleteCoin = App\Coin::FindOrFail($id);
        if($deleteCoin->is_local){
            $msjError = 'This currency is local, please change the currency to remove it!';
            return view('errors.errorAccount', compact('msjError'));
        }
        $deleteCoin->delete();
        return back(); 
    }


    public function updateCoin(Request $r){
        $updateCoin = App\Coin::FindOrFail($r->id);
        $updateCoin = json_encode($updateCoin); 
        return $updateCoin; 
    }

    public function validateCoin(){
        return request()->validate([
         'name'=>'required',
         'description'=>'required',
         'interestRate'=>'required|numeric',
     ]);
    }

    public function existLocalCurrency($coin){
        $user = App\User::findOrFail(auth()->user()->id);
        if($user->coins->isEmpty()){
            $coin->is_local = true; 
        }
    }
    
    public function update($id){
        $this->validateCoinUpdate();
        $coin = App\Coin::findOrFail($id);
        

        if(request('isLocal')){
            $coinLocal = App\Coin::where('user_id', Auth()->user()->id)
            ->where('is_local', true)->first();

            if($coinLocal->id != $coin->id){
                $coinLocal->is_local = false; 
                $coinLocal->save(); 
            }
        }

        $coin->name = request("nameUpdate"); 
        $coin->description=request('descriptionUpdate'); 
        $coin->rate=request('rateUpdate'); 
        $coin->is_local=request('isLocal'); 

        $coin->save(); 
        return back(); 
    }

    public function validateCoinUpdate(){
        return request()->validate([
         'nameUpdate'=>'required',
         'descriptionUpdate'=>'required',
        
     ]);
    }

}
