<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function subcategories(){
        return $this->hasMany(SubCategory::class);
    }

    public function transactions(){
        return $this->hasMany(Transaction::class);
    }
    public function type(){
        return $this->belongsTo(Coin::class); 
    }

}
